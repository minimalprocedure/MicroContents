copy /Y *.txt   ..\locale\en
copy /Y *.txt   ..\locale\it
copy /Y *.txt   ..\locale\id
copy /Y *.txt   ..\locale\aa
copy /Y *.txt   ..\locale\ab
copy /Y *.txt   ..\locale\af
copy /Y *.txt   ..\locale\am
copy /Y *.txt   ..\locale\ar
copy /Y *.txt   ..\locale\as
copy /Y *.txt   ..\locale\ay
copy /Y *.txt   ..\locale\az
copy /Y *.txt   ..\locale\ba
copy /Y *.txt   ..\locale\be
copy /Y *.txt   ..\locale\bg
copy /Y *.txt   ..\locale\bh
copy /Y *.txt   ..\locale\bi
copy /Y *.txt   ..\locale\bn
copy /Y *.txt   ..\locale\bo
copy /Y *.txt   ..\locale\br
copy /Y *.txt   ..\locale\ca
copy /Y *.txt   ..\locale\co
copy /Y *.txt   ..\locale\cs
copy /Y *.txt   ..\locale\cy
copy /Y *.txt   ..\locale\da
copy /Y *.txt   ..\locale\de
copy /Y *.txt   ..\locale\dz
copy /Y *.txt   ..\locale\el
copy /Y *.txt   ..\locale\eo
copy /Y *.txt   ..\locale\es
copy /Y *.txt   ..\locale\et
copy /Y *.txt   ..\locale\eu
copy /Y *.txt   ..\locale\fa
copy /Y *.txt   ..\locale\fi
copy /Y *.txt   ..\locale\fj
copy /Y *.txt   ..\locale\fo
copy /Y *.txt   ..\locale\fr
copy /Y *.txt   ..\locale\fy
copy /Y *.txt   ..\locale\ga
copy /Y *.txt   ..\locale\gd
copy /Y *.txt   ..\locale\gl
copy /Y *.txt   ..\locale\gn
copy /Y *.txt   ..\locale\gu
copy /Y *.txt   ..\locale\ha
copy /Y *.txt   ..\locale\hi
copy /Y *.txt   ..\locale\hr
copy /Y *.txt   ..\locale\hu
copy /Y *.txt   ..\locale\hy
copy /Y *.txt   ..\locale\ia
copy /Y *.txt   ..\locale\ie
copy /Y *.txt   ..\locale\ik
copy /Y *.txt   ..\locale\in
copy /Y *.txt   ..\locale\is
copy /Y *.txt   ..\locale\iw
copy /Y *.txt   ..\locale\ja
copy /Y *.txt   ..\locale\ji
copy /Y *.txt   ..\locale\jw
copy /Y *.txt   ..\locale\ka
copy /Y *.txt   ..\locale\kk
copy /Y *.txt   ..\locale\kl
copy /Y *.txt   ..\locale\km
copy /Y *.txt   ..\locale\kn
copy /Y *.txt   ..\locale\ko
copy /Y *.txt   ..\locale\ks
copy /Y *.txt   ..\locale\ku
copy /Y *.txt   ..\locale\ky
copy /Y *.txt   ..\locale\la
copy /Y *.txt   ..\locale\ln
copy /Y *.txt   ..\locale\lo
copy /Y *.txt   ..\locale\lt
copy /Y *.txt   ..\locale\lv
copy /Y *.txt   ..\locale\mg
copy /Y *.txt   ..\locale\mi
copy /Y *.txt   ..\locale\mk
copy /Y *.txt   ..\locale\ml
copy /Y *.txt   ..\locale\mn
copy /Y *.txt   ..\locale\mo
copy /Y *.txt   ..\locale\mr
copy /Y *.txt   ..\locale\ms
copy /Y *.txt   ..\locale\mt
copy /Y *.txt   ..\locale\my
copy /Y *.txt   ..\locale\na
copy /Y *.txt   ..\locale\ne
copy /Y *.txt   ..\locale\nl
copy /Y *.txt   ..\locale\no
copy /Y *.txt   ..\locale\oc
copy /Y *.txt   ..\locale\om
copy /Y *.txt   ..\locale\or
copy /Y *.txt   ..\locale\pa
copy /Y *.txt   ..\locale\pl
copy /Y *.txt   ..\locale\ps
copy /Y *.txt   ..\locale\pt
copy /Y *.txt   ..\locale\qu
copy /Y *.txt   ..\locale\rm
copy /Y *.txt   ..\locale\rn
copy /Y *.txt   ..\locale\ro
copy /Y *.txt   ..\locale\ru
copy /Y *.txt   ..\locale\rw
copy /Y *.txt   ..\locale\sa
copy /Y *.txt   ..\locale\sd
copy /Y *.txt   ..\locale\sg
copy /Y *.txt   ..\locale\sh
copy /Y *.txt   ..\locale\si
copy /Y *.txt   ..\locale\sk
copy /Y *.txt   ..\locale\sl
copy /Y *.txt   ..\locale\sm
copy /Y *.txt   ..\locale\sn
copy /Y *.txt   ..\locale\so
copy /Y *.txt   ..\locale\sq
copy /Y *.txt   ..\locale\sr
copy /Y *.txt   ..\locale\ss
copy /Y *.txt   ..\locale\st
copy /Y *.txt   ..\locale\su
copy /Y *.txt   ..\locale\sv
copy /Y *.txt   ..\locale\sw
copy /Y *.txt   ..\locale\ta
copy /Y *.txt   ..\locale\te
copy /Y *.txt   ..\locale\tg
copy /Y *.txt   ..\locale\th
copy /Y *.txt   ..\locale\ti
copy /Y *.txt   ..\locale\tk
copy /Y *.txt   ..\locale\tl
copy /Y *.txt   ..\locale\tn
copy /Y *.txt   ..\locale\to
copy /Y *.txt   ..\locale\tr
copy /Y *.txt   ..\locale\ts
copy /Y *.txt   ..\locale\tt
copy /Y *.txt   ..\locale\tw
copy /Y *.txt   ..\locale\uk
copy /Y *.txt   ..\locale\ur
copy /Y *.txt   ..\locale\uz
copy /Y *.txt   ..\locale\vi
copy /Y *.txt   ..\locale\vo
copy /Y *.txt   ..\locale\wo
copy /Y *.txt   ..\locale\xh
copy /Y *.txt   ..\locale\yo
copy /Y *.txt   ..\locale\zh
copy /Y *.txt   ..\locale\zu

copy /Y *.png   ..\locale\en
copy /Y *.png   ..\locale\it
copy /Y *.png   ..\locale\id
copy /Y *.png   ..\locale\aa
copy /Y *.png   ..\locale\ab
copy /Y *.png   ..\locale\af
copy /Y *.png   ..\locale\am
copy /Y *.png   ..\locale\ar
copy /Y *.png   ..\locale\as
copy /Y *.png   ..\locale\ay
copy /Y *.png   ..\locale\az
copy /Y *.png   ..\locale\ba
copy /Y *.png   ..\locale\be
copy /Y *.png   ..\locale\bg
copy /Y *.png   ..\locale\bh
copy /Y *.png   ..\locale\bi
copy /Y *.png   ..\locale\bn
copy /Y *.png   ..\locale\bo
copy /Y *.png   ..\locale\br
copy /Y *.png   ..\locale\ca
copy /Y *.png   ..\locale\co
copy /Y *.png   ..\locale\cs
copy /Y *.png   ..\locale\cy
copy /Y *.png   ..\locale\da
copy /Y *.png   ..\locale\de
copy /Y *.png   ..\locale\dz
copy /Y *.png   ..\locale\el
copy /Y *.png   ..\locale\eo
copy /Y *.png   ..\locale\es
copy /Y *.png   ..\locale\et
copy /Y *.png   ..\locale\eu
copy /Y *.png   ..\locale\fa
copy /Y *.png   ..\locale\fi
copy /Y *.png   ..\locale\fj
copy /Y *.png   ..\locale\fo
copy /Y *.png   ..\locale\fr
copy /Y *.png   ..\locale\fy
copy /Y *.png   ..\locale\ga
copy /Y *.png   ..\locale\gd
copy /Y *.png   ..\locale\gl
copy /Y *.png   ..\locale\gn
copy /Y *.png   ..\locale\gu
copy /Y *.png   ..\locale\ha
copy /Y *.png   ..\locale\hi
copy /Y *.png   ..\locale\hr
copy /Y *.png   ..\locale\hu
copy /Y *.png   ..\locale\hy
copy /Y *.png   ..\locale\ia
copy /Y *.png   ..\locale\ie
copy /Y *.png   ..\locale\ik
copy /Y *.png   ..\locale\in
copy /Y *.png   ..\locale\is
copy /Y *.png   ..\locale\iw
copy /Y *.png   ..\locale\ja
copy /Y *.png   ..\locale\ji
copy /Y *.png   ..\locale\jw
copy /Y *.png   ..\locale\ka
copy /Y *.png   ..\locale\kk
copy /Y *.png   ..\locale\kl
copy /Y *.png   ..\locale\km
copy /Y *.png   ..\locale\kn
copy /Y *.png   ..\locale\ko
copy /Y *.png   ..\locale\ks
copy /Y *.png   ..\locale\ku
copy /Y *.png   ..\locale\ky
copy /Y *.png   ..\locale\la
copy /Y *.png   ..\locale\ln
copy /Y *.png   ..\locale\lo
copy /Y *.png   ..\locale\lt
copy /Y *.png   ..\locale\lv
copy /Y *.png   ..\locale\mg
copy /Y *.png   ..\locale\mi
copy /Y *.png   ..\locale\mk
copy /Y *.png   ..\locale\ml
copy /Y *.png   ..\locale\mn
copy /Y *.png   ..\locale\mo
copy /Y *.png   ..\locale\mr
copy /Y *.png   ..\locale\ms
copy /Y *.png   ..\locale\mt
copy /Y *.png   ..\locale\my
copy /Y *.png   ..\locale\na
copy /Y *.png   ..\locale\ne
copy /Y *.png   ..\locale\nl
copy /Y *.png   ..\locale\no
copy /Y *.png   ..\locale\oc
copy /Y *.png   ..\locale\om
copy /Y *.png   ..\locale\or
copy /Y *.png   ..\locale\pa
copy /Y *.png   ..\locale\pl
copy /Y *.png   ..\locale\ps
copy /Y *.png   ..\locale\pt
copy /Y *.png   ..\locale\qu
copy /Y *.png   ..\locale\rm
copy /Y *.png   ..\locale\rn
copy /Y *.png   ..\locale\ro
copy /Y *.png   ..\locale\ru
copy /Y *.png   ..\locale\rw
copy /Y *.png   ..\locale\sa
copy /Y *.png   ..\locale\sd
copy /Y *.png   ..\locale\sg
copy /Y *.png   ..\locale\sh
copy /Y *.png   ..\locale\si
copy /Y *.png   ..\locale\sk
copy /Y *.png   ..\locale\sl
copy /Y *.png   ..\locale\sm
copy /Y *.png   ..\locale\sn
copy /Y *.png   ..\locale\so
copy /Y *.png   ..\locale\sq
copy /Y *.png   ..\locale\sr
copy /Y *.png   ..\locale\ss
copy /Y *.png   ..\locale\st
copy /Y *.png   ..\locale\su
copy /Y *.png   ..\locale\sv
copy /Y *.png   ..\locale\sw
copy /Y *.png   ..\locale\ta
copy /Y *.png   ..\locale\te
copy /Y *.png   ..\locale\tg
copy /Y *.png   ..\locale\th
copy /Y *.png   ..\locale\ti
copy /Y *.png   ..\locale\tk
copy /Y *.png   ..\locale\tl
copy /Y *.png   ..\locale\tn
copy /Y *.png   ..\locale\to
copy /Y *.png   ..\locale\tr
copy /Y *.png   ..\locale\ts
copy /Y *.png   ..\locale\tt
copy /Y *.png   ..\locale\tw
copy /Y *.png   ..\locale\uk
copy /Y *.png   ..\locale\ur
copy /Y *.png   ..\locale\uz
copy /Y *.png   ..\locale\vi
copy /Y *.png   ..\locale\vo
copy /Y *.png   ..\locale\wo
copy /Y *.png   ..\locale\xh
copy /Y *.png   ..\locale\yo
copy /Y *.png   ..\locale\zh
copy /Y *.png   ..\locale\zu
